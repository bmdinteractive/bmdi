<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress


 */
define('WP_SITEURL','https://bmdinteractive.com');
define('WP_HOME','https://bmdinteractive.com');
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bmdinteractive');

/** MySQL database username */
define('DB_USER', 'bmdinteractive');

/** MySQL hostname */
define('DB_HOST', 'localhost:3306');
// /** MySQL database password */
// define('DB_PASSWORD', 'hX2S&3##w1A');

//SPINUP
define('DB_PASSWORD', '3s5jvEr3R7KVOKbjbSPlZXCi'); 
// /** MySQL hostname */

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('DISABLE_WP_CRON', true);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#-;$-G!^4U-,n75e;.z#3R<DMagnuP-WM#`~S+|-y,+~mjOn[=C78o)9k6CRY-V[');
define('SECURE_AUTH_KEY',  'qS%M[&9@J#R~/iNXbYxbid=~0I?8U(sP[n|WBRm~<s{IQ57wQ kX>5ksX:J,v4ZO');
define('LOGGED_IN_KEY',    'ML 9YpsG+Z}X+zFQ!ZX41nDZKL{.}{WMS1qfI-+YZ4+5rzz//_-s/TP`GOnnSdY.');
define('NONCE_KEY',        'y`/8jX1invajO*t7$gPeBwFb=8ILok{`ZTZyHx?(ZmNl-vZxk `4,_i|k+L6[;V^');
define('AUTH_SALT',        'XWrJm!:mp9d+xBrB|Mt*n;B#.keA1+ON6+6+bpY.O|3tR/kE$-+rGaEzbc>j*tO;');
define('SECURE_AUTH_SALT', ')!-(]^TX-e&[k(T/{%,ofDM$Fe~-Wam4Ck+s%q|,;]le2l-+08dh7lh!nbqaCwg/');
define('LOGGED_IN_SALT',   '8hzA2)8&f)eWG]+VrFT!^;#w&-@FE2! QS`MsqtR~t/]9|qJ*8<-%gAz|*9I4qf#');
define('NONCE_SALT',       'T6Z;+{ir:R|cAbDg[!u9F+2>o)BkJK3QE<!W#bYD#=bTU~@MEq4cSJ(|_;]bl=&z');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpbmdi_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
