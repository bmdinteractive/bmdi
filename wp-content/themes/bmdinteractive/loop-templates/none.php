<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package birdstrap
 */

?>

<section class="no-results not-found">

	<header class="page-header">

		<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'birdstrap' ); ?></h1>

	</header><!-- .page-header -->

	<div class="page-content">

<?php
if ( is_search() ) :
?>

		<p><?php esc_html_e( 'Sorry, but nothing matched your search. Please try again with some different keywords.', 'birdstrap' ); ?></p>

<?php
else :
?>

		<p><?php esc_html_e( 'It seems we can\'t find what you\'re looking for. Try a search below to find what you\'re looking for.', 'birdstrap' ); ?></p>

<?php
endif;
?>

		<?php get_search_form(); ?>

	</div><!-- .page-content -->

</section><!-- .no-results -->
