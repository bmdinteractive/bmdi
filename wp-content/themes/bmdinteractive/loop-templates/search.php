<?php
/**
 * Search results partial template.
 *
 * @package birdstrap
 */

?>
<article <?php post_class( 'search' ); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
		'</a></h2>' ); ?>

<?php
if ( 'post' == get_post_type() ) :
?>

		<div class="entry-meta">

			<?php birdstrap_posted_on(); ?>

		</div><!-- .entry-meta -->

<?php
endif;
?>

	</header><!-- .entry-header -->

	<div class="entry-summary">

		<?php the_excerpt(); ?>

	</div><!-- .entry-summary -->

	<footer class="entry-footer">

<?php
if ( 'post' == get_post_type() ) :
?>

		<?php birdstrap_entry_footer(); ?>

<?php
endif;
?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
