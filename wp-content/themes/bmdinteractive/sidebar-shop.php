<?php
/**
 * The sidebar containing the widget area for WooCommerce pages.
 *
 * @package birdstrap
 */

if ( ! is_active_sidebar( 'shop-sidebar' ) ) {
	return;
}
?>

<div class="widget-area" role="complementary">

	<?php dynamic_sidebar( 'shop-sidebar' ); ?>

</div><!-- .widget-area -->
