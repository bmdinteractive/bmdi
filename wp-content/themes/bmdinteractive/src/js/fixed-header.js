/**
 * File fixed-header.js
 *
 * This file is only enqueued if `fixedHeader` is true in themeconfig.json
 */

import simplystuck from 'simplystuck';

// Class changes based on scroll
simplystuck('masthead', {
	classTarget: 'body', // where classes should be applied
	stuckTarget: '#content', // which element triggers the "stuck" state
	scrolledOffset: 50,
});
