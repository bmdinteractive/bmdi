/**
 * Simply Stuck!
 *
 * A simple custom sticky header script that just adds classes.
 *
 * @version 1.1.0
 * @author Cory Hughart <cory@blackbird.digital>
 */

import 'raf/polyfill'; // requestAnimationFrame polyfill
import $ from 'jquery';

class SimplyStuck {

	constructor(targetID, options) {
		this.$target = $(`#${targetID}`);

		this.defaultProps = {
			classTarget: false, // Element that classes should be applied to, or false to use target
			stuckClass: 'stuck', // Class applied when stuck
			unstuckClass: 'unstuck', // Class applied when unstuck
			stuckInitClass: 'stuck-ready', // Class applied after js init
			scrolledClass: 'scrolled', // Class applied if page has been scrolled
			stuckOffset: 0, // Offset in px for when stuckClass should be applied
			scrolledOffset: 0, // Offset in px for when scrolledClass should be applied
			stuckTarget: false, // Element that dictates when target is stuck after scrolling past
			recalcOnResize: true, // Recalculate target's offset from top on window resize
		};
		this.props = Object.assign({}, this.defaultProps, options);

		this.$classTarget = !this.props.classTarget ? this.$target : $(this.props.classTarget);
		this.$stuckTarget = !this.props.stuckTarget ? false : $(this.props.stuckTarget);

		this.targetTop = 0; // Target element is always assumed to be at the top of the page
		this.stuckTargetPos = 0;
		this.requestedFrame = null;

		this.init();
	}

	init() {
		//this.targetTop = this.$target.offset().top; // TODO: when to recalc?
		this.scrollY = window.scrollY || window.pageYOffset;

		this.calculateOffsets();

		this.$classTarget.addClass(this.props.stuckInitClass);
		this.$classTarget.addClass(this.props.unstuckClass);

		this.calculateStuckness();

		window.addEventListener('scroll', this.debounceScroll.bind(this));

		if (this.recalcOnResize) {
			window.addEventListener('resize', this.calculateOffsets.bind(this));
		}
	}

	debounceScroll() {

		this.scrollY = window.scrollY || window.pageYOffset;

		if (this.requestedFrame) {
			window.cancelAnimationFrame(this.requestedFrame);
		}

		this.requestedFrame = window.requestAnimationFrame(this.calculateStuckness.bind(this));
	}

	calculateStuckness() {
		const {scrollY, targetTop, stuckTargetPos, $target, $classTarget, $stuckTarget} = this;
		const {stuckClass, unstuckClass, scrolledClass, stuckOffset, scrolledOffset} = this.props;

		const isScrolled = $classTarget.is(`.${scrolledClass}`);
		const isStuck = $classTarget.is(`.${stuckClass}`);

		// Stuck top
		//const stuckY = $stuckTarget && $stuckTarget.length ? targetTop + stuckTargetPos + stuckOffset - $target.height() : targetTop + stuckTargetPos + stuckOffset;

		// Stuck bottom
		const stuckY = targetTop - $target.height() + stuckTargetPos + stuckOffset;

		if (!isScrolled && this.scrollY > scrolledOffset) {
			$classTarget.addClass(scrolledClass);
		}
		else if (isScrolled && this.scrollY <= scrolledOffset) {
			$classTarget.removeClass(scrolledClass);
		}

		if (!isStuck && this.scrollY >= stuckY) {
			$classTarget.addClass(stuckClass);
			$classTarget.removeClass(unstuckClass);
		}
		else if (isStuck && this.scrollY < stuckY ) {
			$classTarget.removeClass(stuckClass);
			$classTarget.addClass(unstuckClass);
		}
	}

	calculateOffsets() {
		if (this.$stuckTarget && this.$stuckTarget.length) {
			// Stuck at bottom
			//this.stuckTargetPos = this.$stuckTarget.offset().top + this.$stuckTarget.height();

			// Stuck at top
			this.stuckTargetPos = this.$stuckTarget.offset().top;
		}
	}

	cleanup() {
		window.removeEventListener('scroll', this.debounceScroll);
		window.removeEventListener('resize', this.calculateOffsets);
	}
}

export default function simplystuck(target, options) {
	return new SimplyStuck(target, options)
}
