/**
 * File blocks.js
 *
 * This script is for creating or otherwise altering Gutenberg blocks and is
 * only relevant in the back-end Gutenberg editor.
 *
 * NOTE: See `/inc/editor.php` for enqueueing UI styles.
 *
 * For WP core block CSS, see `src/scss/modules/_block.scss`.
 */

/**
 * Import your custom Gutenberg blocks from the blocks folder here
 */
// import './blocks/custom-block.js';

const { registerBlockStyle } = wp.blocks;

/**
 * Plain List Style
 *
 * Adds has-style-plain-list class to ul/ol elements
 */
const plainListStyle = {
	name: 'plain-list',
	label: 'Plain List',
};
registerBlockStyle('core/list', plainListStyle);

/**
 * Add code to UN-register styles/blocks should happen after domReady
 */
wp.domReady( () => {
	// Example: unregister a specific core block
	//wp.blocks.unregisterBlockType( 'core/verse' );

	// Example: unregister a specific core block style
	//wp.blocks.unregisterBlockStyle( 'core/quote', 'large' );
});
