/**
 * File theme.js
 *
 * Main theme scripts which will be loaded at the bottom of every page
 *
 * You can import JS modules from NPM by name, and you can also import from
 * `/src/js/modules/` & `/src/js/vendor/` by name as well (no need for the path)
 *
 * Remember that these will load on every page. If you need a page-dependent
 * script, create a separate file in the root of the `/src/js/` folder and
 * enqueue it in `/inc/enqueue.php`.
 *
 * This file has access to a global `config` variable with the contents of your
 * themeconfig.json file in it, plus a few handy things like `themeurl`.
 */


// Remove html element no-js class, add js
document.documentElement.className = document.documentElement.className.replace( /(?:^|\s)no-js(?!\S)/g, '') + ' js';


// Bootstrap
import 'bootstrap';


/**
 * Lazysizes
 *
 * Image optimization with a blur-in transition that speeds up page load
 *
 * Use php function get_lazy_attachment_image() to generate <img> tags
 * compatible with lazysizes
 */
//import 'lazysizes';

import './theme/skip-link-focus-fix.js';
