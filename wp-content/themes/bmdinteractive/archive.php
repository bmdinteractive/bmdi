<?php
/**
 * The default template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package birdstrap
 */

get_header();
?>

<div class="container">
	<div class="row">

		<div class="content-col">

<?php
if ( have_posts() ) :
?>

			<header class="page-header page-header--archive">
				<?php the_archive_title( '<h1 class="site-title">', '</h1>' ); ?>
				<?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
			</header><!-- .page-header -->

			<div class="archive-list">

<?php
	while ( have_posts() ) : the_post();
		// Support post formats, otherwise look for custom post type tempalate
		$type = get_post_type();
		if ( $type == 'post' ) {
			$type = get_post_format();
		}
?>

				<?php get_template_part( 'loop-templates/excerpt', $type ); ?>

<?php
	endwhile;
?>
			</div><!-- .archive-list -->

			<?php birdstrap_pagination(); ?>
<?php
else :
?>

			<?php get_template_part( 'loop-templates/none' ); ?>

<?php
endif;
?>

		</div><!-- .content-col -->

		<div class="col-md-<?php sidebar_columns(); ?> <?php sidebar_position(); ?> sidebar-col">

			<?php get_sidebar( 'blog' ); ?>

		</div><!-- .sidebar-col -->

	</div><!-- .row -->
</div><!-- .container -->

<?php
get_footer();
