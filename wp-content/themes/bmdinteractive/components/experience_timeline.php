<?php
/**
 * Component: experience timeline
 */

$experiences = get_prop('experiences', '');
?>
<div class="experience-timeline">
<?php 
foreach($experiences as $experience):
	$company = get_the_title( $experience );
	$location = get_field('location', $experience );
	$job_title = get_field('job_title', $experience );
	$clients = get_field('clients', $experience );
	$responsibilities = get_field('responsibilities', $experience );
	$accomplishments = get_field('accomplishments', $experience );
	$start_date = get_field('start_date', $experience );
	$end_date = (get_field('end_date', $experience )) ? get_field('end_date', $experience ) : 'Present';
?>
<div class="timeline-item">
	<div class="exp-time-location">
		<div class="exp-time"><?php echo $start_date; ?> - <?php echo $end_date; ?></div>
		<div class="exp-location"><?php echo $location; ?></div>
	</div>
	<div class="exp-detail">
		<div class="exp-company"><?php echo $company; ?></div>
		<div class="exp-title">(<?php echo $job_title; ?>)</div>
		<div class="exp-responsibilities"><?php echo $responsibilities; ?></div>
		<div class="exp-clients">
			<strong>Clients:</strong><br>
			<?php echo $clients; ?>
		</div>
	</div>
</div>
<?php endforeach; 
wp_reset_query();
?>
  </div>