<?php
/**
 * The template for displaying all woocommerce pages.
 *
 * @package birdstrap
 */

get_header();
?>

<div class="container">
	<div class="row">

		<div class="col content-col">

<?php
$template_name = '\archive-product.php';
$args = array();
$template_path = '';
$default_path = untrailingslashit( plugin_dir_path(__FILE__) ) . '\woocommerce';

if ( is_singular( 'product' ) ) {
	woocommerce_content();
}
//For ANY product archive, Product taxonomy, product search or /shop landing page etc Fetch the template override;
elseif ( file_exists( $default_path . $template_name ) )
	{
	wc_get_template( $template_name, $args, $template_path, $default_path );
}
//If no archive-product.php template exists, default to catchall;
else  {
	woocommerce_content( );
}
?>

		</div><!-- .content-col -->

		<div class="col-md-<?php sidebar_columns(); ?> <?php sidebar_position(); ?> sidebar-col">

			<?php get_sidebar( 'shop' ); ?>

		</div><!-- .sidebar-col -->

	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>
