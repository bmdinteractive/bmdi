<?php
/**
 * Menu declarations and utility functions
 *
 * @package birdstrap
 */

/**
 * Load custom WordPress nav walker.
 */
require dirname(__FILE__) . '/class-bootstrap-wp-navwalker.php';

/**
 * Register Menus
 */
if ( ! function_exists( 'birdstrap_register_menus' ) ) :
function birdstrap_register_menus() {
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'birdstrap' ),
		'utility' => __( 'Utility Menu', 'birdstrap' ),
		'footer' => __( 'Footer Menu', 'birdstrap' ),
	) );
}
endif;
add_action( 'init', 'birdstrap_register_menus' );
