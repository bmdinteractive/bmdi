<?php
/**
 * SVG Uploads
 *
 * @package birdstrap
 */

/**
 * Safe SVG plugin filters
 */

/**
 * Do not filter animate tags from SVG
 */
function allow_svg_tags( $tags ) {
	$tags[] = 'animate';
	return $tags;
}
add_filter( 'svg_allowed_tags', 'allow_svg_tags' );



/**
 * ==========
 * DEPRECATED
 * ==========
 *
 * Use Safe SVG plugin instead!
 * https://wordpress.org/plugins/safe-svg/
 */

if ( ! function_exists( 'birdstrap_mime_types' ) ) :
/**
 * Enable media upload mime types
 */
function birdstrap_svg_mime_type( $mimes ) {
	// Enable SVG
	$mimes['svg'] = 'image/svg+xml';
	$mimes['svgz'] = 'image/svg+xml';
	return $mimes;
}
endif;
//add_filter('upload_mimes', 'birdstrap_svg_mime_type');

/**
 * Extract width & height data from SVG uploads
 * https://wordpress.stackexchange.com/a/256701
 */
function birdstrap_svg_metadata( $data, $id ){

	$attachment = get_post( $id ); // Filter makes sure that the post is an attachment
	$mime_type = $attachment->post_mime_type; // The attachment mime_type

	// If the attachment is an svg
	if ( $mime_type == 'image/svg+xml' ){

		// If the svg metadata is empty or the width is empty or the height is empty
		// then get the attributes from xml.

		if ( empty( $data ) || empty( $data['width'] ) || empty( $data['height'] ) ) {
			$xml = simplexml_load_file( wp_get_attachment_url( $id ) );
			$attr = $xml->attributes();
			$viewbox = explode( ' ', $attr->viewBox );
			$data['width'] = isset( $attr->width ) && preg_match( '/\d+/', $attr->width, $value ) ? (int) $value[0] : ( count($viewbox) == 4 ? (int) $viewbox[2] : null );
			$data['height'] = isset( $attr->height ) && preg_match( '/\d+/', $attr->height, $value) ? (int) $value[0] : ( count($viewbox) == 4 ? (int) $viewbox[3] : null );
		}

		if ( empty( $data['file'] ) ) {
			$data['file'] = _wp_relative_upload_path( get_attached_file( $id ) );
		}
	}

	return $data;
}
//add_filter( 'wp_update_attachment_metadata', 'birdstrap_svg_metadata', 10, 2 );

/**
 * View SVG images in Media Library
 */
function birdstrap_svg_admin_preview( $response, $attachment, $meta ) {
	if ( $response['mime'] == 'image/svg+xml' ) {
		$possible_sizes = apply_filters( 'image_size_names_choose', array(
			'thumbnail' => __( 'Thumbnail' ),
			'medium'    => __( 'Medium' ),
			'large'     => __( 'Large' ),
			'full'      => __( 'Full Size' ),
		) );

		$sizes = array();

		foreach ( $possible_sizes as $size => $label ) {
			$sizes[ $size ] = array(
				'height'      => get_option( "{$size}_size_w", 2000 ),
				'width'       => get_option( "{$size}_size_h", 2000 ),
				'url'         => $response['url'],
				'orientation' => 'portrait'
			);
		}

		$response['sizes'] = $sizes;
		$response['icon'] = $response['url'];
	}

	return $response;
}
//add_filter( 'wp_prepare_attachment_for_js', 'birdstrap_svg_admin_preview', 10, 3 );
