<?php
/**
 * Custom functions that output information based on the current context.
 *
 * Functions that don't rely on context should go in extras.php
 *
 * @package birdstrap
 */

if ( ! function_exists( 'birdstrap_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time.
 */
function birdstrap_posted_on() {
	$time_template = get_the_time( 'U' ) === get_the_modified_time( 'U' ) ?
	' <time class="published updated" datetime="%1$s">%2$s</time>' :
	' <time class="published" datetime="%1$s">%2$s</time> <time class="updated" datetime="%3$s">%4$s</time>';

	$time_string = sprintf( $time_template,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	printf(
		' <span class="posted-on"><span class="label">%s</span> %s</span>',
		esc_html_x( 'Posted on', 'post date', 'birdstrap' ),
		$time_string
	); // WPCS: XSS OK.
}
endif;


if ( ! function_exists( 'birdstrap_posted_by' ) ) :
/**
 * Prints HTML with meta information for the current author.
 */
function birdstrap_posted_by( $link = false ) {
	$byline_template = $link ?
	' <span class="byline"><span class="label">%1$s</span> <span class="author vcard"><a class="url fn" href="%3$s">%2$s</a></span></span>' :
	' <span class="byline"><span class="label">%1$s</span> <span class="author vcard"><span class="fn">%2$s</span></span></span>';

	printf(
		$byline_template,
		esc_html_x( 'by', 'post author', 'birdstrap' ),
		esc_html( get_the_author() ),
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) )
	); // WPCS: XSS OK.
}
endif;


if ( ! function_exists( 'birdstrap_cats' ) ) :
/**
 * Prints HTML with category list.
 */
function birdstrap_cats() {
	/* translators: used between list items, there is a space after the comma */
	$categories_list = get_the_category_list( esc_html__( ', ', 'birdstrap' ) );
	if ( $categories_list && birdstrap_categorized_blog() ) {
		printf(
			' <span class="taxonomy-list categories"><span class="label">%s</span> %s</span>',
			esc_html__( 'Posted in', 'birdstrap' ),
			$categories_list
		); // WPCS: XSS OK.
	}
}
endif;


if ( ! function_exists( 'birdstrap_tags' ) ) :
/**
 * Prints HTML with tags list.
 */
function birdstrap_tags() {
	/* translators: used between list items, there is a space after the comma */
	$tags_list = get_the_tag_list( '', esc_html__( ', ', 'birdstrap' ), '' );
	if ( $tags_list ) {
		printf(
			' <span class="taxonomy-list tags"><span class="label">%s</span> %s</span>',
			esc_html__( 'Tagged', 'birdstrap' ),
			$tags_list
		); // WPCS: XSS OK.
	}
}
endif;


if ( ! function_exists( 'birdstrap_tax' ) ) :
/**
 * Prints HTML with custom taxonomy list.
 */
function birdstrap_tax( $taxonomy, $label = null ) {
	/* translators: used between list items, there is a space after the comma */
	$tags_list = get_the_term_list( get_the_ID(), $taxonomy, '', esc_html__( ', ', 'birdstrap' ), '' );
	if ( $tags_list ) {
		if ( $label === null ) {
			$tax = get_taxonomy( $taxonomy );
			$label = $tax ? $tax->name : '';
		}
		printf(
			' <span class="taxonomy-list %s"><span class="label">%s</span> %s</span>',
			esc_attr( $taxonomy ),
			$label,
			$tags_list
		); // WPCS: XSS OK.
	}
}
endif;


if ( ! function_exists( 'birdstrap_comments' ) ) :
/**
 * Prints HTML for comments.
 */
function birdstrap_comments() {
	echo ' <span class="comments-link">';
	comments_popup_link( esc_html__( 'Leave a comment', 'birdstrap' ), esc_html__( '1 Comment', 'birdstrap' ), esc_html__( '% Comments', 'birdstrap' ) );
	echo '</span>';
}
endif;


if ( ! function_exists( 'birdstrap_edit_link' ) ) :
/**
 * Prints HTML for edit link.
 */
function birdstrap_edit_link() {
	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'birdstrap' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		' <span class="edit-link">',
		'</span>'
	);
}
endif;


if ( ! function_exists( 'birdstrap_entry_footer' ) ) :
/**
 * Prints HTML with all of the above meta information.
 */
function birdstrap_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		birdstrap_cats();
		birdstrap_tags();
	}
	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		birdstrap_comments();
	}
	birdstrap_edit_link();
}
endif;


/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function birdstrap_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'birdstrap_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );
		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );
		set_transient( 'birdstrap_categories', $all_the_cool_cats );
	}
	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so components_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so components_categorized_blog should return false.
		return false;
	}
}


/**
 * Flush out the transients used in birdstrap_categorized_blog.
 */
function birdstrap_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'birdstrap_categories' );
}
add_action( 'edit_category', 'birdstrap_category_transient_flusher' );
add_action( 'save_post',     'birdstrap_category_transient_flusher' );
