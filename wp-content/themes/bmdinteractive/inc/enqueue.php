<?php
/**
 * Birdstrap enqueue scripts/styles
 *
 * @package birdstrap
 */

if ( ! function_exists( 'birdstrap_scripts' ) ) :
/**
 * Load theme's JavaScript and Style sources.
 */
function birdstrap_scripts() {
	$config = birdstrap_get_theme_config();

	wp_enqueue_style( 'birdstrap', get_stylesheet_directory_uri() . '/css/theme.css', array(), cache_buster( '/css/theme.css' ), false );
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css2?family=Roboto:wght@300;700;900&display=swap', array(), cache_buster( '/css/theme.css' ), false );

	// Move jQuery to footer
	wp_dequeue_script( 'jquery-core' );
	wp_dequeue_script( 'jquery' );
	wp_dequeue_script( 'jquery-migrate' );
	wp_enqueue_script( 'jquery-core', false, array(), false, true );
	wp_enqueue_script( 'jquery-migrate', false, array(), false, true );
	wp_enqueue_script( 'jquery', false, array('jquery-core'), false, true );

	wp_enqueue_script( 'birdstrap', get_stylesheet_directory_uri() . '/js/theme.js', array('jquery'), cache_buster( '/js/theme.js' ), true );
	wp_localize_script( 'birdstrap', 'config', $config );

	// Webfonts
	birdstrap_enqueue_fonts();

	// Theme config
	if ( ! empty( $config['fixedHeader'] ) ) {
		wp_enqueue_script( 'birdstrap-fixed-header', get_stylesheet_directory_uri() . '/js/fixed-header.js', array('jquery'), cache_buster( '/js/theme.js' ), true );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
endif;
add_action( 'wp_enqueue_scripts', 'birdstrap_scripts' );

if ( ! function_exists( 'birdstrap_noscript_fallback' ) ) :
function birdstrap_noscript_fallback() {
?>
<noscript>
	<!-- load fonts if async font loading is unavailable -->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/css/fonts.css" />
</noscript>
<?php
}
endif;
add_action( 'wp_footer', 'birdstrap_noscript_fallback', 99 );
