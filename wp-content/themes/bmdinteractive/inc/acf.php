<?php
/**
 * Advanced Custom Fields functions
 *
 * Use this file to bake custom fields into the theme, add options pages, and
 * any other ACF-related customizations
 */

/**
 * Options pages
 */
if ( ! function_exists( 'acf_add_options_pages' ) ) :
function add_acf_options_pages() {
	acf_add_options_page(array(
		'page_title' => 'Site Options',
		'menu_title' => 'Site Options',
		'menu_slug' => 'site-options',
		'position' => 30,
		'icon_url' => 'dashicons-admin-generic',
		'page_id' => 'options', // Slug to use when getting values, ex: get_field( 'foo', 'options')
		//'capability' => 'edit_posts',
	));
}
endif;
//add_action( 'admin_menu', 'add_acf_options_pages', 10 );


/**
 * Restrict access to ACF fields editor
 */
if ( ! function_exists( 'birdstrap_restrict_acf' ) ) :
function birdstrap_restrict_acf( $allowed ) {
	return current_user_can( 'manage_options' );
}
endif;
add_filter( 'acf/settings/show_admin', 'birdstrap_restrict_acf' );

/**
 * Register ACF Gutenberg blocks
 */
function acf_register_blocks() {

	if ( ! function_exists( 'acf_register_block_type' ) ) return;

	acf_register_block_type( array(
		'name' => 'experience_timeline',
		'title' => 'Experience Timeline',
		'description' => 'List experiences in timeline format',
		'keywords' => array( 'link', 'list' ),
		'icon' => 'admin-links',
		'render_callback' => 'block_render_experience_timeline',
		'category' => 'custom',
	) );
}
add_action( 'acf/init', 'acf_register_blocks' );

function block_render_experience_timeline( $block, $content = '', $is_preview = false, $post_id = 0 ) {
	the_component( 'experience_timeline', get_fields(), $block );
}

