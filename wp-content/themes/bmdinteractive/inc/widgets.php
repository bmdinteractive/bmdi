<?php
/**
 * Widget area declarations and utility functions
 *
 * @package birdstrap
 */

/**
 * Include custom widget classes
 */
require dirname(__FILE__) . '/widgets/class-pagenav-widget.php';


if ( ! function_exists( 'birdstrap_widgets_init' ) ) :
/**
 * Initialize sidebars
 */
function birdstrap_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'birdstrap' ),
		'id'            => 'blog-sidebar',
		'description'   => 'Blog sidebar widget area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Page Sidebar', 'birdstrap' ),
		'id'            => 'page-sidebar',
		'description'   => 'Page sidebar widget area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer', 'birdstrap' ),
		'id'            => 'footer',
		'description'   => 'Widget area below main content and above footer',
		'before_widget' => '<div id="%1$s" class="footer-widget %2$s ' . birdstrap_widget_columns( 'footerfull' ) . '">',
		'after_widget'  => '</div><!-- .footer-widget -->',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	// Shop sidebar (only if WooCommerce is active)
	if ( class_exists( 'WooCommerce' ) ) {
		register_sidebar( array(
			'name'          => __( 'WooCommerce Sidebar', 'birdstrap' ),
			'id'            => 'shop-sidebar',
			'description'   => 'Shop sidebar widget area',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}
}
endif;
add_action( 'widgets_init', 'birdstrap_widgets_init' );

/**
 * Count number of widgets in a sidebar
 */
function birdstrap_get_widgets_count( $sidebar_id ) {
	$sidebars_widgets = wp_get_sidebars_widgets();
	return (int) count( (array) $sidebars_widgets[ $sidebar_id ] );
}

/**
 * Add classes to a widget area so widgets can be displayed one, two, three, or four per row
 */
if ( ! function_exists( 'birdstrap_widget_columns' ) ) :
function birdstrap_widget_columns( $sidebar_id ) {
	// If loading from front page, consult $_wp_sidebars_widgets rather than options
	// to see if wp_convert_widget_settings() has made manipulations in memory.
	global $_wp_sidebars_widgets;
	if ( empty( $_wp_sidebars_widgets ) ) {
		$_wp_sidebars_widgets = get_option( 'sidebars_widgets', array() );
	}

	$sidebars_widgets_count = $_wp_sidebars_widgets;

	if ( isset( $sidebars_widgets_count[ $sidebar_id ] ) ) {
		$widget_count = count( $sidebars_widgets_count[ $sidebar_id ] );
		$widget_classes = 'widget-count-' . count( $sidebars_widgets_count[ $sidebar_id ] );
		if ( $widget_count % 4 == 0 || $widget_count > 6 ) {
			// Four widgets per row if there are exactly four or more than six
			$widget_classes .= ' col-md-3';
		}
		elseif ( 6 == $widget_count ) {
			// If two widgets are published
			$widget_classes .= ' col-md-2';
		}
		elseif ( $widget_count >= 3 ) {
			// Three widgets per row if there's three or more widgets
			$widget_classes .= ' col-md-4';
		}
		elseif ( 2 == $widget_count ) {
			// If two widgets are published
			$widget_classes .= ' col-md-6';
		}
		elseif ( 1 == $widget_count ) {
			// If just on widget is active
			$widget_classes .= ' col-md-12';
		}
		return $widget_classes;
	}
}
endif;
