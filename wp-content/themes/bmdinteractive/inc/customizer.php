<?php
/**
 * Birdstrap Theme Customizer
 *
 * @package birdstrap
 */

if ( ! function_exists( 'birdstrap_customize_register' ) ) :
/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function birdstrap_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
endif;
add_action( 'customize_register', 'birdstrap_customize_register' );


if ( ! function_exists( 'birdstrap_theme_customize_register' ) ) :
/**
 * Register individual settings through customizer's API.
 *
 * @param WP_Customize_Manager $wp_customize Customizer reference.
 */
function birdstrap_theme_customize_register( $wp_customize ) {

	// Footer Logo
	$wp_customize->add_setting( 'footer_logo');

	$wp_customize->add_control(
		/**
		 * NOTE: We're using the Media control here so that the image is stored
		 * as a media ID instead of by it's full URL, which which will change
		 * between the staging & live servers
		 */
		new WP_Customize_Media_Control(
			$wp_customize,
			'footer_logo',
			array(
				'label'       => __( 'Footer Logo', 'birdstrap' ),
				'description' => __( 'This version of the logo will be used in the footer. If left blank, the logo above will be used.', 'birdstrap' ),
				'section'     => 'title_tagline',
				'settings'    => 'footer_logo',
				//'type'        => 'image', // NOTE: this results in the stored value being a URL
				'mime_type'   => 'image',
				'priority'    => 9,
				'button_labels' => array(
					'select'       => __( 'Select logo' ),
					'change'       => __( 'Change logo' ),
					'default'      => __( 'Default' ),
					'remove'       => __( 'Remove' ),
					'placeholder'  => __( 'No logo selected' ),
					'frame_title'  => __( 'Select logo' ),
					'frame_button' => __( 'Choose logo' ),
				)
			)
		)
	);
}
endif;
add_action( 'customize_register', 'birdstrap_theme_customize_register' );


if ( ! function_exists( 'birdstrap_customize_preview_js' ) ) :
/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function birdstrap_customize_preview_js() {
	wp_enqueue_script( 'birdstrap_customizer', get_template_directory_uri() . '/js/customizer.js',
		array( 'customize-preview' ), '20171220', true );
}
endif;
add_action( 'customize_preview_init', 'birdstrap_customize_preview_js' );


if ( ! function_exists( 'birdstrap_custom_logo_class' ) ) :
/**
 * Filter custom logo with correct classes.
 *
 * @param string $html Markup.
 *
 * @return mixed
 */
function birdstrap_custom_logo_class( $html ) {

	$html = str_replace( 'class="custom-logo"', 'class="logo"', $html );
	$html = str_replace( 'class="custom-logo-link"', 'class="navbar-brand custom-logo-link"', $html );
	$html = str_replace( 'alt=""', 'title="Home" alt="logo"' , $html );

	return $html;
}
endif;
add_filter( 'get_custom_logo', 'birdstrap_custom_logo_class' );


if ( ! function_exists( 'birdstrap_get_footer_logo' ) ) :
/**
 * Get footer logo as if calling `the_custom_logo()`
 */
function birdstrap_get_footer_logo() {
	$footer_logo = get_theme_mod( 'footer_logo' );
	if ( $footer_logo ) {
		$meta = wp_get_attachment_metadata( $footer_logo );
		$html = '<a href="" class="footer-logo-link" rel="home" itemprop="url">';
		$html .= '<img class="footer-logo" src="';
		$html .= wp_get_attachment_image_src( $footer_logo, 'medium' )[0];
		$html .= '" alt="';
		$html .= get_post_meta( $footer_logo, '_wp_attachment_image_alt', true );
		$html .= '" itemprop="logo" width="'.$meta['width'].'" height="'.$meta['height'].'" />';
		$html .= '</a>';
		return $html;
	}
	elseif ( has_custom_logo() ) {
		return get_custom_logo();
	}
	else {
		return '';
	}
}
endif;


/**
 * Select option sanitization function
 *
 * @param  String $input
 * @param  Object $setting
 * @return String          $input or default setting
 */
function birdstrap_theme_slug_sanitize_select( $input, $setting ){

	// Input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only
	$input = sanitize_key($input);

	// Get the list of possible select options
	$choices = $setting->manager->get_control( $setting->id )->choices;

	// Return input if valid or return default option
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}
