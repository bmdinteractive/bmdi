<?php
/**
 * Global settings
 */

// Used to display color palette in Gutenberg and other color customization contexts
// Default: array( 'primary', 'secondary', 'tertiary', 'success', 'info', 'warning', 'danger', 'light', 'dark' );
$theme_colors = array( 'primary', 'secondary', 'tertiary', 'success', 'info', 'warning', 'danger', 'light', 'dark', 'white', 'black' );
