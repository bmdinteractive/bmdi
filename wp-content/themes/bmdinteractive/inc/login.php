<?php
/**
 * Login
 */

// custom css
function birdstrap_login_styles() {
	wp_enqueue_style( 'birdstrap', get_stylesheet_directory_uri() . '/css/theme.css', array(), cache_buster( '/css/theme.css' ), false );

	if ( has_custom_logo() ) :
		$logo_id = get_theme_mod('custom_logo');
		$url = wp_get_attachment_image_src( $logo_id, 'large' );
?>
<style type="text/css">
	#login h1 a, .login h1 a {
		background-image: url(<?php echo $url[0]; ?>);
	}
</style>
<?php
	endif;
}
add_action( 'login_enqueue_scripts', 'birdstrap_login_styles', 10 );

// changing the logo link from wordpress.org to your site
function birdstrap_login_url() {  return home_url(); }
add_filter( 'login_headerurl', 'birdstrap_login_url' );

// changing the alt text on the logo to show your site name
function birdstrap_login_title() { return get_option( 'blogname' ); }
add_filter( 'login_headertext', 'birdstrap_login_title' );
