<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Functions that rely on context should go in template-tags.php
 *
 * @package birdstrap
 */

/**
 * Get image tag for use with Lazysizes JS
 */
function get_lazy_attachment_image( $attachment_id, $size = 'preview', $icon = false, $attr = '' ) {
	$default_attr = array( 'class' => 'lazyload blur-up' );
	$attr = wp_parse_args( $default_attr, $attr );
	$img = wp_get_attachment_image( $attachment_id, $size, $icon, $attr );
	$lq_src = wp_get_attachment_image_src( $attachment_id, 'preview' );
	$lazy_img = str_replace( array( ' src=', ' srcset=', '<img' ), array( ' data-src=', ' data-srcset=', '<img src="'.$lq_src[0].'"' ), $img );

	return '<noscript>'.$img.'</noscript>'.$lazy_img;
}
