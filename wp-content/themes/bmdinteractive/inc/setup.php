<?php
/**
 * Theme basic setup
 *
 * @package birdstrap
 */


// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) {
	$content_width = 1200; /* pixels */
}

/**
 * Checking if a function exists makes our theme pluggable, meaning a child
 * theme can override the function with its own
 */
if ( ! function_exists( 'birdstrap_theme_support' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function birdstrap_theme_support() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on birdstrap, use a find and replace
	 * to change 'birdstrap' to the name of your theme in all the template files
	 */
	//load_theme_textdomain( 'birdstrap', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head.
	 */
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );



	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Support featured images
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * Custom image sizes
	 *
	 * Use a media regeneration plugin after adding/changing these
	 */
	//add_image_size( 'preview', 200 );
	//add_image_size( 'backdrop', 1400 );

	/*
	 * Support widget edit icons in customizer
	 */
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * Support for Post Formats
	 * See http://codex.wordpress.org/Post_Formats
	 */
	/*
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );
	*/


	/**
	 * Customizer Features
	 */

	// Custom background color/image
	/*
	add_theme_support( 'custom-background',
		apply_filters( 'birdstrap_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);
	*/

	// Custom logo
	add_theme_support( 'custom-logo' );


	/**
	 * Support Gutenberg/Blocks
	 */

	// Adds default frontend CSS to some blocks such as:
	// separator, blockquote, table
	add_theme_support( 'wp-block-styles' );

	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	// Adds CSS on the front-end to keep embeds at specific aspect ratios
	add_theme_support( 'responsive-embeds' );

	/*
	 * Add theme colors to Gutenberg
	 *
	 * You can specify your own array of color names to extract from the CSS
	 * in the _settings.php file ($theme_colors)
	 */
	$colors = birdstrap_get_color_palette();

	// Disable Custom Colors
	add_theme_support( 'disable-custom-colors' );

	// Editor Color Palette
	add_theme_support( 'editor-color-palette', $colors );
}
endif;
add_action( 'after_setup_theme', 'birdstrap_theme_support' );

/**
 * Remove block styles from frontend
 */
function birdstrap_remove_block_styles() {
	wp_dequeue_style( 'wp-block-library' );
}
//add_action( 'wp_enqueue_scripts', 'birdstrap_remove_block_styles', 200 );
