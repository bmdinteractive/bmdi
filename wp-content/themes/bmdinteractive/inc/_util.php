<?php
/**
 * Utility functions
 */


/**
 * Cache utils
 */

/**
 * Attempts to provide a cache-busting string based on the file's last
 * modification time. Falls back to the theme version number.
 *
 * TODO: Can we devise a solution that doesn't require touching the filesystem
 * every pageload?
 *
 * @param $theme_filepath Filepath relative to the theme root,
 *                        should start with a "/"
 */
function cache_buster( $theme_filepath ) {
	$the_theme = wp_get_theme();
	$the_version = $the_theme->get( 'Version' );

	if ( empty( $theme_filepath ) ) return $the_version;

	return filemtime( get_template_directory() . $theme_filepath );
}


/**
 * Layout utils
 */

/**
 * Get width of content in columns
 *
 * @since 0.12.0
 * @return int Number of Bootstrap columns
 */
function get_content_columns() {
	global $sidebar_cols;
	return 12 - $sidebar_cols;
}
function content_columns() {
	echo get_content_columns();
}

/**
 * Get width of sidebar in columns
 *
 * @deprecated 0.17.0 This is now handled via CSS
 * @since 0.12.0
 * @return int Number of Bootstrap columns
 */
function get_sidebar_columns() {
	return 4;
}
function sidebar_columns() {
	echo get_sidebar_columns();
}

/**
 * Get sidebar position class
 *
 * @deprecated 0.17.0 This is now handled via CSS
 * @since 0.12.0
 * @return string Sidebar position order class
 */
function get_sidebar_position() {
	global $sidebar_position;
	return $sidebar_position;
}
function sidebar_position() {
	echo get_sidebar_position();
}

/**
 * Get bootstrap container type class
 *
 * @deprecated 0.17.0 Just use the plain .container class
 * @since 0.12.0
 * @return string Container class
 */
function get_container() {
	return 'container';
}
function container() {
	echo get_container();
}


/**
 * Theme config utils
 */

/**
 * Get Birdstrap theme config
 *
 * @since 0.17.0
 * @return array Theme configuration
 */
function birdstrap_get_theme_config() {
	$config = get_transient( 'birdstrap_config' );
	if ( $config === false ) {
		$config = birdstrap_read_theme_config();
		if ( $config === false ) {
			$config = array(
				'fontawesome' => array('solid', 'regular', 'light', 'brands'),
			);
		}
		set_transient( 'birdstrap_config', $config, 60 );
	}

	$config['themeurl'] = get_stylesheet_directory_uri();

	return $config;
}

function birdstrap_read_theme_config() {
	$config_file = file_get_contents( get_stylesheet_directory() . '/themeconfig.json');
	$config = false;
	if ( $config_file !== false ) {
		$config = json_decode( birdstrap_strip_comments( $config_file ), true );
	}

	return $config;
}

/**
 * Strip comments from a string (//, /*, #)
 *
 * @since 0.17.0
 * @param string $str Input string with comments
 * @return string String stripped of comments
 */
function birdstrap_strip_comments( $str ) {
	return preg_replace('~(" (?:[^"\\\\] | \\\\\\\\ | \\\\")*+ ") | \# [^\v]*+ | // [^\v]*+ | /\* .*? \*/~xs', '$1', $str);
}


/**
 * Theme color utils
 */

/**
 * Get theme color palette
 *
 * @since 0.12.0
 * @param boolean $name_array Alternate array of colors to read from theme.css
 * @return array Theme color palette
 */
function birdstrap_get_color_palette( $name_array = false ) {
	global $theme_colors;

	$color_name_array = $name_array ? $name_array : $theme_colors;

	// Load color palette from transient if available
	$hash = md5( serialize( $color_name_array ) );
	$colors = get_transient( 'birdstrap_color_palette_'.$hash );
	if ( $colors === false ) {
		//error_log( 'no colors transient found' );
		$colors = birdstrap_get_theme_colors( $color_name_array );
	}
	else {
		// Check that transient-loaded color palette is the same as what we asked for
		try {
			$transient_name_array = wp_list_pluck( $colors, 'slug' );
			if ( ! empty( array_diff( $color_name_array, $transient_name_array ) ) ) {
				//error_log( 'colors transient was outdated' );
				$colors = birdstrap_get_theme_colors( $color_name_array );
			}
		}
		catch (Exception $error) {
			//error_log( 'colors transient was malformed' );
			$colors = birdstrap_get_theme_colors( $color_name_array );
		}
	}

	return $colors;
}

/**
 * Returns a Gutenberg-compatible array of colors
 *
 * @since 0.12.0
 * @param array $color_name_array Optional array of color names
 * @return array Colors array
 */
function birdstrap_get_theme_colors( $color_name_array ) {
	$raw_colors = birdstrap_read_theme_colors( $color_name_array );

	$colors = array();
	foreach ($raw_colors as $name => $value) {
		$colors[] = array(
			'name' => ucwords(str_replace('-', ' ', $name)), // TODO: does this need i18n?
			'slug' => $name,
			'color' => $value,
		);
	}

	set_transient( 'birdstrap_color_palette', $colors, MONTH_IN_SECONDS );

	return $colors;
}

/**
 * Reads theme colors from theme.css
 *
 * See: https://developer.wordpress.org/reference/functions/get_file_data/
 *
 * @since 0.12.0
 * @param array $color_name_array Array of color names we're looking for
 * @return array Variable name as key, color hex as value
 */
function birdstrap_read_theme_colors( $name_array = false ) {
	global $theme_colors;

	$color_name_array = $name_array ? $name_array : $theme_colors;

	$fp = fopen( get_stylesheet_directory() . '/css/theme.css', 'r' );
	// Pull only the first 8kiB of the file in.
	$file_data = fread( $fp, 8192 );
	// PHP will close file handle, but we are good citizens.
	fclose( $fp );

	$raw_colors = array();

	foreach ( $color_name_array as $name ) {
		if ( preg_match( '/--'.$name.':(#[^;]+);/', $file_data, $matches ) ) {
			$raw_colors[$name] = $matches[1];
		}
	}

	return $raw_colors;
}


/**
 * Enqueue utils
 */

/**
 * Enqueue fonts
 *
 * This is a util so that it can be used on front end and back end.
 * Note that webfonts are added via themeconfig.json.
 *
 * @since 0.18.0
 */
function birdstrap_enqueue_fonts( $deps = array() ) {
	$config = birdstrap_get_theme_config();

	wp_enqueue_script( 'birdstrap-fonts', get_stylesheet_directory_uri() . '/js/fonts.js', $deps, cache_buster( '/js/fonts.js' ), false );
	wp_localize_script( 'birdstrap-fonts', 'config', $config );
}
