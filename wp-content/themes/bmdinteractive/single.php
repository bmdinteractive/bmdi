<?php
/**
 * The template for displaying all single posts.
 *
 * @package birdstrap
 */

get_header();
?>

<div class="container">
	<div class="row">

		<div class="col content-col">

<?php
while ( have_posts() ) : the_post();
// Support post formats, otherwise look for custom post type tempalate
	$type = get_post_type();
	if ( $type == 'post' ) {
		$type = get_post_format();
	}
?>
			<?php get_template_part( 'loop-templates/content', $type ); ?>

			<?php birdstrap_post_nav(); ?>

<?php
	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() && get_comments_number() ) :
		comments_template();
	endif;
endwhile;
?>

		</div><!-- .content-col -->

		<div class="col-md-<?php sidebar_columns(); ?> <?php sidebar_position(); ?> sidebar-col">

			<?php get_sidebar( 'blog' ); ?>

		</div><!-- .sidebar-col -->

	</div><!-- .row -->
</div><!-- .container -->

<?php
get_footer();
