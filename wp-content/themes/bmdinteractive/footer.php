<?php
/**
 * The template for displaying the footer.
 *
 * Displays the closing of <main> and footer
 *
 * @package birdstrap
 */

?>
	</main>
	<footer id="colophon" class="site-footer">
		<div class="container">

			<?php wp_nav_menu(
				array(
					'theme_location'  => 'footer',
					'depth'           => 1, // set to 0 for submenus
					'container'       => null,
					'menu_class'      => 'nav nav-menu nav-menu--footer',
					'fallback_cb'     => '',
					'menu_id'         => 'footer-menu',
					'walker'          => new WP_Bootstrap_Navwalker(),
				)
			); ?>

			<div class="site-info">
				<p class="copyright">©<?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>, all rights reserved.</p>
			</div><!-- .site-info -->

		</div><!-- .container  -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
