<?php
/**
 * Template Name: Experience
 *
 * @package birdstrap
 */

get_header();
?>

<div class="container">
	<div class="row">

		<div class="content-col">

<?php
while ( have_posts() ) : the_post();
?>
			<?php get_template_part( 'loop-templates/content', 'page' ); ?>

<?php
endwhile;
?>

		</div><!-- .content-col -->

	</div><!-- .row -->
</div><!-- .container -->

<?php
get_footer();
