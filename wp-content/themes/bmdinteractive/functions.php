<?php
/**
 * Birdstrap functions and definitions
 *
 * @package birdstrap
 */

/**
 * Global Settings
 */
require get_template_directory() . '/inc/_settings.php';

/**
 * Utility functions
 */
require get_template_directory() . '/inc/_util.php';

/**
 * Theme setup and custom theme supports
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Filters & actions for native WordPress functionality
 */
require get_template_directory() . '/inc/wordpress.php';

/**
 * Component functions
 */
require get_template_directory() . '/inc/components.php';

/**
 * ACF customizations
 */
require get_template_directory() . '/inc/acf.php';

/**
 * SVG support
 */
require get_template_directory() . '/inc/svg.php';

/**
 * Register menus
 */
require get_template_directory() . '/inc/menus.php';

/**
 * Register widget area
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Enqueue scripts and styles
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Load Login customizations
 */
require get_template_directory() . '/inc/login.php';

/**
 * Custom template tags for this theme
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom template tags for this theme
 */
require get_template_directory() . '/inc/pagination.php';

/**
 * Customizer additions
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom Comments file
 */
require get_template_directory() . '/inc/custom-comments.php';

/**
 * Load Jetpack compatibility file
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load WooCommerce functions
 */
require get_template_directory() . '/inc/woocommerce.php';

/**
 * Load Editor functions
 */
require get_template_directory() . '/inc/editor.php';
