<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and opens <main>
 *
 * @package birdstrap
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<meta name="mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'birdstrap' ); ?></a>

<div class="site" id="page">
	<div id="masthead" class="site-header">
		<nav class="navbar navbar-expand-md navbar-light">
			<div class="container">

<?php
if ( has_custom_logo() ) :
?>

				<!--<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>-->
				<?php the_custom_logo(); ?>

<?php
else :
?>

				<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><?php bloginfo( 'name' ); ?></a>

<?php
endif;
?>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle Navigation">
					<i class="fa fa-bars"></i>
				</button>

				<div id="main-nav" class="collapse navbar-collapse main-menu">
					<div class="main-menu__content">
						<?php wp_nav_menu(
							array(
								'theme_location'  => 'primary',
								'container'       => null,
								'menu_class'      => 'navbar-nav nav-menu nav-menu--primary',
								'fallback_cb'     => '',
								'menu_id'         => 'primary-menu',
								'walker'          => new WP_Bootstrap_Navwalker(),
							)
						); ?>
						<?php wp_nav_menu(
							array(
								'theme_location'  => 'utility',
								'container'       => null,
								'menu_class'      => 'navbar-nav nav-menu nav-menu--utility',
								'fallback_cb'     => '',
								'menu_id'         => 'utility-menu',
								'walker'          => new WP_Bootstrap_Navwalker(),
							)
						); ?>
					</div>
				</div><!-- #main-nav -->

			</div><!-- .container -->
		</nav><!-- .navbar -->
	</div><!-- #masthead -->
	<main id="content" tabindex="-1">
