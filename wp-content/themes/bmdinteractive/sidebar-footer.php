<?php
/**
 * Sidebar setup for footer full.
 *
 * @package birdstrap
 */

?>

<?php if ( is_active_sidebar( 'footer' ) ) : ?>

<div class="row">

	<?php dynamic_sidebar( 'footer' ); ?>

</div><!-- .row -->

<?php endif; ?>
